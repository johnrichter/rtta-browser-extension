import * as React from 'react';
import { TReactFCR, TReactFCP } from '../../typings/utils';
import { STORES, IStores } from './root';

export const StoresContext: React.Context<IStores> = React.createContext<IStores>(STORES);

export function useStores(): IStores {
    return React.useContext(StoresContext);
}

export function StoresProvider(props: TReactFCP<{ value?: IStores }>): TReactFCR {
    const defaultStores: IStores = useStores();
    const providerStores: IStores = props.value || defaultStores;
    return <StoresContext.Provider value={providerStores}>{props.children}</StoresContext.Provider>;
}
