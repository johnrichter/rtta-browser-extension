import * as shortid from 'shortid';
import { observable, runInAction, computed, action, IObservableValue } from 'mobx';
import { create, Jss, GenerateId } from 'jss';
import { jssPreset, createGenerateClassName } from '@material-ui/styles';
import { Theme } from '@material-ui/core';
import { TThemeVariant, DARK_THEME, LIGHT_THEME } from '../theme/variants';
import { TWindowDimensions } from '../../typings/ui';

const JSS_SEED: string = process.env.NODE_ENV !== 'production' ? '' : shortid.generate();

export class UIStore {
    @observable.ref private _jssInstance!: Jss;
    @observable.ref private _jssClassNameGenerator!: GenerateId;
    @observable private _theme!: Theme;
    @observable private _themeVariant!: IObservableValue<TThemeVariant>;
    @observable private _windowDimensions!: TWindowDimensions;

    constructor(theme: TThemeVariant = 'light') {
        runInAction('Initialize State', () => {
            this._jssClassNameGenerator = createGenerateClassName({ productionPrefix: 'j', seed: JSS_SEED });
            this._jssInstance = create({
                createGenerateId: this._createJSSClassNameGenerator,
                plugins: [...jssPreset().plugins],
                insertionPoint: document.getElementById('jss-insertion-point') || undefined,
            });
            this._themeVariant = observable.box(theme);
            this.changeTheme(theme);

            this._windowDimensions = observable({ width: window.innerWidth, height: window.innerHeight });
            window.addEventListener('resize', this._handleWindowResize);
        });
    }

    @action public cleanup(): void {
        window.removeEventListener('resize', this._handleWindowResize);
    }

    @computed public get jss(): Jss {
        return this._jssInstance;
    }

    @computed public get jssClassNameGenerator(): GenerateId {
        return this._jssClassNameGenerator;
    }

    @computed public get themeVariant(): TThemeVariant {
        return this._themeVariant.get();
    }

    @computed public get theme(): Theme {
        return this._theme;
    }

    @action.bound public changeTheme(variant: TThemeVariant): void {
        this._themeVariant.set(variant);
        switch (variant) {
            case 'dark':
                this._theme = DARK_THEME;
                break;
            case 'light':
            default:
                this._theme = LIGHT_THEME;
                break;
        }
    }

    @computed public get window(): TWindowDimensions {
        return this._windowDimensions;
    }

    private _createJSSClassNameGenerator = (): GenerateId => {
        return this._jssClassNameGenerator;
    }

    @action private _handleWindowResize = (e: UIEvent): void => {
        this._windowDimensions.width = window.innerWidth;
        this._windowDimensions.height = window.innerHeight;
    }
}

export const UI_STORE: UIStore = new UIStore();
