import {
    Palette, PaletteOptions, PaletteColor, SimplePaletteColorOptions
} from '@material-ui/core/styles/createPalette';

declare module '@material-ui/core/styles/createPalette' {
    export interface Palette {
        tooltip: PaletteColor;
        info: PaletteColor;
        success: PaletteColor;
        warning: PaletteColor;
    }

    export interface PaletteOptions {
        tooltip: SimplePaletteColorOptions;
        info: SimplePaletteColorOptions;
        success: SimplePaletteColorOptions;
        warning: SimplePaletteColorOptions;
    }
}
