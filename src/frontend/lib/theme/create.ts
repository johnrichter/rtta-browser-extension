import { default as MUICreatePalette, Palette, PaletteOptions } from '@material-ui/core/styles/createPalette';
import { default as MUICreateTypopgraphy, TypographyOptions } from '@material-ui/core/styles/createTypography';

export function CreatePalette(palette: PaletteOptions): Palette {
    const muiPalette: Palette = MUICreatePalette(palette);
    return {
        ...muiPalette,
        tooltip: muiPalette.augmentColor(palette.tooltip),
        info: muiPalette.augmentColor(palette.info),
        success: muiPalette.augmentColor(palette.success),
        warning: muiPalette.augmentColor(palette.warning)
    };
}

export function CreateTypopgraphy(palette: Palette): TypographyOptions {
    return MUICreateTypopgraphy(palette, {});
}
