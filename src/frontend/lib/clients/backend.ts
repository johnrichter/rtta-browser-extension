import { ClientIDs } from '../../../shared/runtime/identifiers';
import { Message } from '../../../shared/runtime/messages';

export class BackendClient {
    private _client: chrome.runtime.Port;

    constructor() {
        this._client = chrome.runtime.connect(ClientIDs.POPUP);
        this._client.onMessage.addListener(this._handleMessage);
    }

    public sendMessage(msg: Message): void {

    }

    private _handleMessage = (message: Message, port: chrome.runtime.Port): void => {

    };
}

export const BACKEND_CLIENT: BackendClient = new BackendClient();
