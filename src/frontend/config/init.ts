import { configure } from 'mobx';

/**
 * Everything is global here so that initialization can happen before any other imports are inspected and/or executed
 */

/**
 * Verify all environment variables that we need exist
 */
const { REACT_APP_NAME }: NodeJS.ProcessEnv = process.env;
if (!REACT_APP_NAME || REACT_APP_NAME.length === 0) throw new Error('Missing app name');

/**
 * This forces that actions are always executed in a Mobx Action. E.g. eliminates human errors
 */
configure({ enforceActions: 'always', isolateGlobalState: true });
