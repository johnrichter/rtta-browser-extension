import { Theme as MUITheme } from '@material-ui/core';
import { makeStyles as muiCoreMakeStyles } from '@material-ui/core';
import { StylesHook } from '@material-ui/styles/makeStyles';
import { Styles, ClassNameMap, ClassKeyOfStyles, WithStylesOptions } from '@material-ui/styles/withStyles';

/**
 * Styled with Hooks
 */

/**
 * Return type of `makeStyles()`
 */
export type MakeStyles<ClassKey extends string = string, Props extends object = {}> = StylesHook<
    Styles<MUITheme, Props extends never ? {} : Props, ClassKey>
>;

/**
 * Return type of the call to the hook in render, i.e. `makeStyles()()`
 */
export type UseStyles<ClassKey extends string = '', Props extends {} = {}> = ClassNameMap<
    ClassKeyOfStyles<Styles<MUITheme, Props extends never ? {} : Props, ClassKey>>
>;

/**
 * An alias function to make working with the order of template arguments easier for makeStyles
 */
export function makeStyles<ClassKey extends string = string, Props extends object = {}>(
    styles: Styles<MUITheme, Props, ClassKey>,
    options?: Omit<WithStylesOptions<MUITheme>, 'withTheme'>,
): StylesHook<Styles<MUITheme, Props, ClassKey>> {
    return muiCoreMakeStyles<MUITheme, Props, ClassKey>(styles, options);
}
