import * as React from 'react';
import { ThreatAssessment } from '../../shared/security';
import { TReactUSR } from '../typings/utils';
import { Message } from '../../shared/runtime/messages';

export function useThreatAssessment(): ThreatAssessment {
    const [threatAssessment, setThreatAssessment]: TReactUSR<ThreatAssessment> = React.useState<ThreatAssessment>(
        { urls: [] }
    );
    React.useEffect(() => {
        function handleResponse(response: Message | undefined) {
            if (response) {
                console.log(JSON.stringify(response));
                switch (response.type) {
                    case 'THREAT_ASSESSMENT':
                        console.log('received threat assessment', JSON.stringify(response));
                        setThreatAssessment(response.assessment);
                        break;
                    default:
                        break;
                }
            }
        }
        chrome.runtime.sendMessage({ type: 'GET_THREAT_ASSESSMENT', context: 'all' }, handleResponse);
    }, []);
    return threatAssessment;
}
