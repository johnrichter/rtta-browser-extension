import { ThreatAssessmentContext, ThreatAssessment } from '../security';

export type ChromeSendResposeFn = (response: Message) => void;
export type Message = UrlFoundOnPageMsg | ThreatAssessmentMsg | GetThreatAssessmentMsg;

export interface UrlFoundOnPageMsg {
    type: 'URL_FOUND_ON_PAGE';
    url: string;
}

export interface ThreatAssessmentMsg {
    type: 'THREAT_ASSESSMENT';
    context: ThreatAssessmentContext;
    assessment: ThreatAssessment;
}

export interface GetThreatAssessmentMsg {
    type: 'GET_THREAT_ASSESSMENT';
    context: ThreatAssessmentContext;
}


