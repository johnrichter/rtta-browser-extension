
export enum ClientIDs {
    WATCHDOG = 'watchdog',
    POPUP = 'popup'
}

export const EXTENSION_ID: string = chrome.runtime.id;
