import { THREAT_ASSESSMENT_STORE } from '../lib/stores/threatAssessment';
import { ChromeRuntimeLifecycle } from './runtime';
import { MessageSender } from '../lib/messageSender';
import { Message, ThreatAssessmentMsg, ChromeSendResposeFn } from '../../shared/runtime/messages';
import { ThreatAssessment } from '../../shared/security';

export class WatchdogDaemon implements ChromeRuntimeLifecycle {

    public onConnect = (port: chrome.runtime.Port): void => {
        if (!port.sender) return;

        const sender: MessageSender = MessageSender.fromChromeMessageSender(port.sender);
        if (!sender.isValid) {
            console.log(`watchdogd: invalid sender -> ${JSON.stringify(port.sender)}`);
            return;
        }

        console.log(`watchdogd: new connection from ${sender.id}`);

        port.onMessage.addListener((message: Message): void => {
            this._handleMessage(sender, message, port.postMessage.bind(port));
        })
    };

    public onMessage = (
        message: Message, chromeSender: chrome.runtime.MessageSender, sendResponse: ChromeSendResposeFn
    ): boolean => {
        const sender: MessageSender = MessageSender.fromChromeMessageSender(chromeSender);
        if (!sender.isValid) {
            console.log(`watchdogd: invalid sender -> ${JSON.stringify(sender)}`);
            return false;
        }
        console.log(`watchdogd: new message from ${sender.id}`);
        return this._handleMessage(sender, message, sendResponse);
    }

    private _handleMessage(sender: MessageSender, message: Message, sendResponse: ChromeSendResposeFn): boolean {
        console.log(`watchdogd: message received from ${sender.id} -> ${JSON.stringify(message)}`);
        let didRespond: boolean = false;

        switch (message.type) {
            case 'URL_FOUND_ON_PAGE':
                THREAT_ASSESSMENT_STORE.addURL(message.url, sender);
                const newUrlAssessment: ThreatAssessmentMsg = {
                    type: 'THREAT_ASSESSMENT',
                    context: 'currentFrame',
                    assessment: { urls: [THREAT_ASSESSMENT_STORE.threatAssessmentForUrl(message.url)] }
                };
                sendResponse(newUrlAssessment);
                didRespond = true;
                this._updateBadgeText(sender.tabID);
                break;
            case 'GET_THREAT_ASSESSMENT':
                const threatAssessment: ThreatAssessmentMsg = {
                    type: 'THREAT_ASSESSMENT',
                    context: message.context,
                    assessment: THREAT_ASSESSMENT_STORE.threatAssessment
                };
                sendResponse(threatAssessment);
                didRespond = true;
                break;
            default:
                break;
        }
        return didRespond;
    }

    private _updateBadgeText(tabId: number): void {
        const assessment: ThreatAssessment = THREAT_ASSESSMENT_STORE.threatAssessmentForTab(tabId);
        const numUrls: number = assessment.urls.length;
        const text: string = numUrls >= 1000 ? '999+' : `${numUrls}`;
        chrome.browserAction.setBadgeText({ text, tabId });
    }
}
