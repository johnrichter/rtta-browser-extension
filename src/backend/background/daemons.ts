import { ChromeRuntime } from './runtime';
import { WatchdogDaemon } from './watchdogd';

const RUNTIME: ChromeRuntime = new ChromeRuntime([
    // new BrowserActionDaemon(),
    new WatchdogDaemon()
]);
RUNTIME.start();
console.log('Runtime: started');
