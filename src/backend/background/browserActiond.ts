import { ChromeRuntimeLifecycle } from './runtime';
import { autorun, IReactionDisposer, IReactionPublic } from 'mobx';
import { CHROME_NAVIGATION_STORE } from '../lib/stores/chromeNavigation';
import { THREAT_ASSESSMENT_STORE } from '../lib/stores/threatAssessment';
import { ThreatAssessment } from '../../shared/security';

export class BrowserActionDaemon implements ChromeRuntimeLifecycle {
    private _disposeBadgeUpdater?: IReactionDisposer;

    constructor() {
        // chrome.tabs.onHighlighted.addListener(this._handleTabHighlighted);
        // this._startBadgeUpdater();
    }

    public onSuspend(): void {
        if (this._disposeBadgeUpdater) this._disposeBadgeUpdater();
    }

    public onSuspendCancelled(): void {
        this._startBadgeUpdater();
    }

    private _handleTabHighlighted = (info: chrome.tabs.HighlightInfo): void => {
        // for (const tabId of Array.isArray(info.tabs) ? info.tabs : [info.tabs]) {
        //     const assessment: ThreatAssessment = THREAT_ASSESSMENT_STORE.threatAssessmentForTab(tabId);
        //     const numUrls: number = assessment.urls.length;
        //     const text: string = numUrls >= 1000 ? '999+' : `${numUrls}`;
        //     console.log(`Setting badge text: ${JSON.stringify({ tabId, numUrls, text })}`);
        //     chrome.browserAction.setBadgeText({ text, tabId });
        // }
    }

    private _startBadgeUpdater(): void {
        if (this._disposeBadgeUpdater) this._disposeBadgeUpdater();
        // this._disposeBadgeUpdater = autorun(
        //     (r: IReactionPublic) => {
        //         const activeTabID: number = CHROME_NAVIGATION_STORE.activeTabInWindow(chrome.windows.WINDOW_ID_CURRENT);
        //         const assessment: ThreatAssessment = THREAT_ASSESSMENT_STORE.threatAssessmentForTab(activeTabID);
        //         const numUrls: number = assessment.urls.length;
        //         const badgeText: string = numUrls >= 1000 ? '999+' : `${numUrls}`;
        //         console.log(`Setting badge text: ${JSON.stringify({
        //             activeTabID, numUrls, badgeText
        //         })}`);
        //         chrome.browserAction.setBadgeText({ text: badgeText });
        //     },
        //     { name: 'Badge Updater' }
        // );
    }
}
