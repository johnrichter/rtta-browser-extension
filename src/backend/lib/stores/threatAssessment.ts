import { observable, action, ObservableMap, computed } from 'mobx';
import { Severity, UrlThreatAssessment, ThreatAssessment, URLThreatAssessment } from '../../../shared/security';
import { MessageSender } from '../messageSender';

const SEVERITIES: Severity[] = ['unknown', 'safe', 'warning', 'danger', 'extreme'];

interface ReporterTab {
    tabID: number;
    frames: number[];
}

interface Reporters {
    windows: number[];
    tabs: ReporterTab[];
}

interface ReportedUrlThreatAssessment extends UrlThreatAssessment {
    reporters: Reporters;
}

export class ThreatAssessmentStore {
    @observable private _urls: ObservableMap<string, ReportedUrlThreatAssessment> = observable.map();

    @action.bound public addURL(url: string, from: MessageSender): void {
        // const reporter: Reporter = {
        //     window: from.windowID,
        //     tab: from.isTab ? { tabID: from.tabID, frames: [from.frameID] } : undefined
        // };
        const assessment: ReportedUrlThreatAssessment | undefined = this._urls.get(url);
        if (assessment === undefined) {
            // TODO: Make this an async callback to the API -- w/e that may be in the future
            this._urls.set(url, {
                url,
                severity: SEVERITIES[Math.round(Math.random() * SEVERITIES.length)],
                occurances: 1,
                reporters: {
                    windows: [from.windowID],
                    tabs: from.isTab ? [{ tabID: from.tabID, frames: [from.frameID] }] : []
                }
            });
        } else {
            assessment.occurances += 1;
            if (!assessment.reporters.windows.includes(from.windowID)) {
                assessment.reporters.windows.push(from.windowID);
            }
            if (from.isTab) {
                const reporterTab: ReporterTab | undefined = assessment.reporters.tabs.find(
                    (r: ReporterTab) => r.tabID === from.tabID
                );
                if (reporterTab !== undefined) {
                    if (!reporterTab.frames.includes(from.frameID)) {
                        reporterTab.frames.push(from.frameID);
                    }
                } else {
                    assessment.reporters.tabs.push({ tabID: from.tabID, frames: [from.frameID] })
                }
            }
        }
    }

    public threatAssessmentForUrl(url: string): UrlThreatAssessment {
        const assessment: ReportedUrlThreatAssessment | undefined = this._urls.get(url);
        if (assessment) {
            const { url: _url, severity, occurances }: ReportedUrlThreatAssessment = assessment;
            return { url: _url, severity, occurances };
        }
        return { url, severity: 'unknown', occurances: 0 };
    }

    public threatAssessmentForTab(id: number): ThreatAssessment {
        const threatAssessment: ThreatAssessment = { urls: [] };

        for (const assessment of this._urls.values()) {
            if (assessment.reporters.tabs.some((r: ReporterTab) => r.tabID === id)) {
                const { url, severity, occurances }: ReportedUrlThreatAssessment = assessment;
                threatAssessment.urls.push({ url, severity, occurances });
            }
        }
        return threatAssessment;
    }

    @computed public get threatAssessment(): ThreatAssessment {
        const urlAsessment: URLThreatAssessment[] = [];
        for (const [url, assessment] of this._urls.entries()) {
            const { severity, occurances }: ReportedUrlThreatAssessment = assessment;
            urlAsessment.push({ url, severity, occurances });
        }
        return { urls: urlAsessment };
    }

    @action.bound public reset(): void {
        this._urls.clear();
    }
}

export const THREAT_ASSESSMENT_STORE: ThreatAssessmentStore = new ThreatAssessmentStore();
