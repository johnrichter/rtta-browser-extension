import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Message } from '../../shared/runtime/messages';
import { ThreatAssessment, Severity, URLThreatAssessment } from '../../shared/security';
import { TReactFCP, TReactFCR, TReactUSR } from '../../frontend/typings/utils';
import { Tooltip } from '@material-ui/core';

// interface ILinkWarningP {
//     anchor: HTMLAnchorElement;
//     severity: Severity;
// }

// function LinkWarning(props: TReactFCP<ILinkWarningP>): TReactFCR {
//     // const ref: React.MutableRefObject<HTMLDivElement | null> = React.useRef(null);
//     // const [didAppend, setDidAppend]: TReactUSR<boolean> = React.useState<boolean>(false);
//     // React.useEffect(() => {
//     //     if (ref.current !== null && !didAppend) {
//     //         console.log('appending anchor');
//     //         ref.current.appendChild(props.anchor);
//     //         setDidAppend(true);
//     //     }
//     // }, [didAppend, setDidAppend, props.anchor]);
//     console.log(JSON.stringify(props.anchor));
//     // const ref: React.MutableRefObject<HTMLDivElement | null> = React.useRef(null);
//     const { attributes, text }: HTMLAnchorElement = props.anchor;

//     return (
//         <Tooltip title={props.severity}>
//             <a ref={ref}>{text}</a>
//         </Tooltip>
//     );
// }

function findDomUrls(): string[] {
    return Array.from(document.getElementsByTagName('a')).map((a: HTMLAnchorElement) => a.href);
}

function stylizeURLs(assessment: ThreatAssessment): void {
    // const matchingAnchors: Map<URLThreatAssessment, HTMLAnchorElement[]> = new Map(
    //     assessment.urls.map((ta: URLThreatAssessment) => [ta, []])
    // );
    // for (const anchor of Array.from(document.getElementsByTagName('a'))) {
    //     const threat: URLThreatAssessment | undefined = assessment.urls.find(
    //         (ta: URLThreatAssessment) => ta.url === anchor.href
    //     );
    //     if (threat !== undefined) {
    //         matchingAnchors.get(threat)!.push(anchor);
    //     }
    // }
    // for (const [threat, anchors] of matchingAnchors.entries()) {
    //     for (const anchor of anchors) {
    //         // const wrapper: HTMLDivElement = document.createElement('div');
    //         // wrapper.setAttribute('class', 'newnewnewnew');
    //         ReactDOM.render(<LinkWarning anchor={anchor} severity={threat.severity} />, anchor.parentElement);
    //         anchor.parentElement!.removeChild(anchor);
    //         // anchor.remove();
    //     }
    // }
}

const port: chrome.runtime.Port = chrome.runtime.connect({ name: 'watchdog' });

port.onMessage.addListener((message: Message): void => {
    console.log('watchdog:', 'message received ->', JSON.stringify(message));
    switch (message.type) {
        case 'THREAT_ASSESSMENT':
            stylizeURLs(message.assessment);
            break;
        default:
            break;
    }
});

window.addEventListener('load', () => {
    // Once the full page has loaded we can look for every url that exists in the DOM
    const urls: string[] = findDomUrls();
    for (const url of urls) {
        console.log(`watchdog: found url: ${url}`);
        port.postMessage({ type: 'URL_FOUND_ON_PAGE', url });
    }
});
